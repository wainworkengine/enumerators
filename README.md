# Module of WainWorkEngine

No additional dependencies

## Getting started

Add this to your Packages\manifest.json
```
"com.wainwork.engine.enumerators": "https://gitlab.com/wainworkengine/enumerators.git#1.0.0",
```


## How to use

Has prepared Enumarators for easy to use.



### Prepared interpolation
```
// Start as coroutine, and this will return interpolated value between [ 0 and 1 ] by duration.
IEnumerator Interpolate(this Action<float> callback, float duration, Action endCallback = null, Func<float> getCurrentTime = null)
```



### Every Random
A special class that returns a random element that has been used less than another
```
string[] names = new string[1];
var everyNames = new EveryRandom<string>(names);
var nextName = everyNames.Next();
```



### Coroutine with data
A special class which work as a usual Coroutine but can return a result of self calculation
```
public IEnumerator WaitCountDown()
{
    var result = "Count down:";
    var count = 1;
    while (count < 10)
    {
        result += $"\n{count}";
        count++;
        yield return new WaitForSeconds(1);
    }
    yield return result;
}

public IEnumerator Work()
{
    var awaitData = new CoroutineWithData<string>(WaitCountDown());
    yield return awaitData.Run();
    var result = awaitData.Result;
}
```